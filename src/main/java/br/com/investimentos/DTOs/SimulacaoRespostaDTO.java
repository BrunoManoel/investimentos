package br.com.investimentos.DTOs;

import br.com.investimentos.models.Simulacao;
import org.springframework.format.annotation.NumberFormat;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;

public class SimulacaoResponseDTO {

    @NumberFormat(style = NumberFormat.Style.PERCENT)
    private Double rendimentoPorMes;
    @NumberFormat(style = NumberFormat.Style.CURRENCY)
    private BigDecimal montante;


    public SimulacaoResponseDTO() {
    }

    public SimulacaoResponseDTO(Double rendimentoPorMes, BigDecimal montante) {
        this.rendimentoPorMes = rendimentoPorMes;
        this.montante = montante;
    }

    public Double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(Double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public BigDecimal getMontante() {
        return montante;
    }

    public void setMontante(BigDecimal montante) {
        this.montante = montante;
    }

    public void converteSimulacaoParaDTO(Simulacao simulacaoProjetada) {

        BigDecimal montante = new BigDecimal(simulacaoProjetada.getValorAplicado());
        double rendimentoPorMes = simulacaoProjetada.getInvestimento().getRendimentoAoMes();
        BigDecimal rendimentoAplicado = new BigDecimal(1 + (rendimentoPorMes/100)).setScale(6, BigDecimal.ROUND_DOWN);

        for (int i = 0; i < simulacaoProjetada.getQuantidadeMeses(); i++) {
            montante = montante.multiply(rendimentoAplicado);
        }

        BigDecimal montanteFormatado = montante.setScale(2, BigDecimal.ROUND_DOWN);

        this.montante = montanteFormatado;
        this.rendimentoPorMes = (rendimentoPorMes);
    }

}
