package br.com.investimentos.DTOs;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;

public class SimulacaoRequestDTO {

    private String nomeInteressado;

    @Email(message = "Email não é valido")
    private String email;

    @Digits(integer = 9, fraction = 2, message = "Preço fora do padrão")
    @DecimalMin(value = "1.0", message = "Valor está fora do normal")
    private Double valorAplicado;

    private int quantidadeMeses;

    public SimulacaoRequestDTO(){}

    public SimulacaoRequestDTO(String nomeInteressado, String email, Double valorAplicado, int quantidadeMeses) {
        this.nomeInteressado = nomeInteressado;
        this.email = email;
        this.valorAplicado = valorAplicado;
        this.quantidadeMeses = quantidadeMeses;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }
}
