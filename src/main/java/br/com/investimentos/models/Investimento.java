package br.com.investimentos.models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Investimento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true)
    private String nome;
    private double rendimentoAoMes;

    public Investimento() {
    }

    public Investimento(int id, String nome, double rendimentoAoMes) {
        this.id = id;
        this.nome = nome;
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}
